<?php
global $post;
$metadata = get_post_meta( $post->ID, 'event_info', true ); ?>
<p>
    <label for="event_info[name]"> <?php _e('Event Name'); ?> </label>
    <br>
    <input type="text" name="event_info[name]" id="event_info[name]" class="regular-text"
           value="<?php if ( is_array( $metadata ) && isset( $metadata['name'] ) ) {
               echo $metadata['name'];
           } ?>">
</p>
<p>
    <label for="event_info[select]"> <?php _e('Public or private event?'); ?> </label>
    <br>
    <select name="event_info[select]" id="event_info[select]">
        <option value="public" <?php selected( $metadata['select'], 'public' ); ?>> <?php _e('Public event') ?> </option>
        <option value="private" <?php selected( $metadata['select'], 'private' ); ?>> <?php _e('Private event') ?> </option>
    </select>
</p>
<p>
    <label for="event_info[date]"> <?php _e('Date') ?> </label>
    <br>
    <input type="date" id="event_info[date]" name="event_info[date]"
           value="<?php if ( is_array( $metadata ) && isset( $metadata['date'] ) ) {
               echo $metadata['date'];
           } ?>" min="<?php echo date_i18n( 'Y-m-d' ) ?>">
</p>
