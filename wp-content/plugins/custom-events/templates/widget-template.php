<?php
echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title'];
foreach ($events as $event) {
    $id = $event -> ID;
    $metadata = get_post_meta( $id, 'event_info', true); ?>
        <p>
            <strong> <?php _e('Event:')?> </strong> <?php echo $metadata['name'] ?> <br>
            <small> <?php
                echo date_i18n( 'D, d F Y', strtotime( $metadata['date'] ) ) ?> </small>
        </p>
    <?php }
echo $args['after_widget'];
