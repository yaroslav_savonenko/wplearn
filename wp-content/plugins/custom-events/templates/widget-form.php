<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"> <?php _e('Title:'); ?> </label>
    <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>"
           name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>"/>
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'quantity' ); ?>"> <?php _e('How many events you want to see:'); ?></label>
    <input type="number" id="<?php echo $this->get_field_id( 'quantity' ); ?>"
           name="<?php echo $this->get_field_name( 'quantity' ); ?>"
           value="<?php echo esc_attr( $quantity ); ?>"/>
</p>

<p>
    <label for="<?php echo $this->get_field_id( 'type' ); ?>"> <?php _e('Type:')?>
        <select class='widefat' id="<?php echo $this->get_field_id( 'type' ); ?>"
                name="<?php echo $this->get_field_name( 'type' ); ?>" type="text">
            <option value='public'<?php echo ( $type == 'public' ) ? 'selected' : ''; ?>>
                <?php _e('Public'); ?>
            </option>
            <option value='private'<?php echo ( $type == 'private' ) ? 'selected' : ''; ?>>
                <?php _e('Private'); ?>
            </option>
        </select>
    </label>
</p>