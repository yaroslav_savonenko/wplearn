<?php
class Custom_Event_Widget extends WP_Widget {
	public function __construct() {
		$widget_options = array(
			'classname'   => 'custom_event_widget',
			'description' => __('This is an Custom Events Widget'),
		);
		parent::__construct( 'custom_event_widget', 'Custom Events Widget', $widget_options );
	}

	public function widget( $args, $instance ) {
		$title              = apply_filters( 'widget_title', $instance['title'] );
		$query              = new WP_Query( array(
		    'post_type' => 'custom_event',
            'meta_query' => array(
                'event_date' => array(
                    'key' => 'date',
                    'value' => time(),
                    'compare' => '>',
                ),
                'event_type' => array(
                    'key' => 'type',
                    'value' => $instance['type'],
                ),
            ),
            'posts_per_page' => $instance['quantity'],
            'orderby' => 'event_date',
            'order' => 'asc',
        ) );
		$events = $query->posts;
        // Show info about events from template. Quantity of events is configurable in widget.
		include plugin_dir_path(__FILE__) . '../../templates/widget-template.php';

	}

	public function form( $instance ) {
		// Check instance and set to default values if instance already exists
		$title    = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$quantity = ! empty( $instance['quantity'] ) ? $instance['quantity'] : '5';
		$type     = ! empty( $instance['type'] ) ? $instance['type'] : '';
		// Include widget form php file.
		include plugin_dir_path(__FILE__) . '../../templates/widget-form.php';

	}

	// Update instance
	public function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['quantity'] = $new_instance['quantity'];
		$instance['type']     = $new_instance['type'];

		return $instance;
	}
}

function custom_events_register_widget() {
	register_widget( 'Custom_Event_Widget' );
}

add_action( 'widgets_init', 'custom_events_register_widget' );