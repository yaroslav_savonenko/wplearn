<?php
function custom_events_shortcode_func( $atts ) {
    // Set variables and default values for shortcode variables
    $params = shortcode_atts( array(
        'quantity' => 1,
        'privacy'  => 'public',
    ), $atts);
    $query = new WP_Query( array(
        'post_type' => 'custom_event',
        'meta_query' => array(
            'event_date' => array(
                'key' => 'date',
                'value' => time(),
                'compare' => '>',
            ),
            'event_type' => array(
                'key' => 'type',
                'value' => $params['privacy'],
            ),
        ),
        'posts_per_page' => $params['quantity'],
        'orderby' => 'event_date',
        'order' => 'asc',
    ) );
    $events = $query->posts;
    $result = '';
    foreach ( $events as $event ) {
        $event_id = $event->ID;
        $metadata = get_post_meta( $event_id, 'event_info', 'true');
        $result .= '<p>' . $metadata['name'] . '<br>' . date_i18n( 'd F Y', strtotime( $metadata['date'] ) ) . '</p>';
    }
    return $result;
}

add_shortcode( 'custom_events', 'custom_events_shortcode_func' );