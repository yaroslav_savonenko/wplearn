<?php
/**
 * Plugin Name: Custom Events
 * Description: Provide a custom post type Events with widget
 * Text Domain: cevents
 * Domain Path: /lang
 */

add_action( 'plugins_loaded', function(){
	load_plugin_textdomain( 'cevents', false, dirname( plugin_basename(__FILE__) ) . '/lang' );
} );

function create_custom_events_post_type() {
    // Labels for post type
	$labels = array(
		'name'               => _x( 'Events', 'post type general name', 'cevents' ),
		'singular_name'      => _x( 'Event', 'post type singular name', 'cevents' ),
		'add_new'            => _x( 'Add New', 'Event', 'cevents' ),
		'add_new_item'       => __( 'Add New Event' ),
		'edit_item'          => __( 'Edit Event' ),
		'new_item'           => __( 'New Event' ),
		'all_items'          => __( 'All Events' ),
		'view_item'          => __( 'View Event' ),
		'search_items'       => __( 'Search events' ),
		'not_found'          => __( 'No events found' ),
		'not_found_in_trash' => __( 'No events found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => __('EventZ'),
	);
	// Arguments for post type custom_event
	$args   = array(
		'labels'        => $labels,
		'description'   => __('Displays events with description and datetime'),
		'public'        => true,
		'menu_position' => 2,
		'supports'      => array( 'title', 'custom-fields', ),
		'has_archive'   => true,
	);
	register_post_type( 'custom_event', $args );
}

add_action( 'init', 'create_custom_events_post_type' );

function custom_taxonomies_custom_events() {
	$labels = array(
		'name'              => _x( 'Event Types', 'taxonomy general name', 'cevents' ),
		'singular_name'     => _x( 'Event Type', 'taxonomy singular name', 'cevents' ),
		'search_items'      => __( 'Search Event Types' ),
		'all_items'         => __( 'All Event Types' ),
		'parent_item'       => __( 'Parent Event Type' ),
		'parent_item_colon' => __( 'Parent Event Type:' ),
		'edit_item'         => __( 'Edit Event Type' ),
		'update_item'       => __( 'Update Event Type' ),
		'add_new_item'      => __( 'Add New Event Type' ),
		'new_item_name'     => __( 'New Event Type' ),
		'menu_name'         => __( 'Event Types' ),
	);
	$args   = array(
		'labels'       => $labels,
		'hierarchical' => true,
	);
	register_taxonomy( 'custom_event_type', 'custom_event', $args );
}

add_action( 'init', 'custom_taxonomies_custom_events', 0 );

function add_custom_event_fields_meta_box() {
	add_meta_box(
		'event_info',
		'Event Info',
		'show_event_info_fields_meta_box',
		'custom_event',
		'normal',
		'high'
	);
}

add_action( 'add_meta_boxes', 'add_custom_event_fields_meta_box' );

function show_event_info_fields_meta_box() {
    //  Create metabox.
	wp_nonce_field( plugin_dir_path(__FILE__),'custom_event_metabox' );
	// Including html metabox template file.
	include plugin_dir_path(__FILE__) . '/templates/event-info-meta-box.php';
 }

function save_event_info_meta( $post_id ) {
	// Verify nonce
	if ( isset( $_POST['custom_event_metabox'] )
	     && ! wp_verify_nonce( $_POST['custom_event_metabox'], plugin_dir_path( __FILE__ ) ) ) {
		return $post_id;
	}
	// Check autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}
	// Check permissions
	if ( isset( $_POST['post_type'] ) ) {
		if ( 'page' === $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return $post_id;
			} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
				return $post_id;
			}
		}
	}

	$old = get_post_meta( $post_id, 'event_info', true );
	if ( isset( $_POST['event_info'] ) ) {
		$new = $_POST['event_info'];
		if ( $new && $new !== $old ) {
			update_post_meta( $post_id, 'event_info', $new );
			update_post_meta( $post_id, 'name', $new['name'] );
			update_post_meta( $post_id, 'date', $new['date'] );
			update_post_meta( $post_id, 'type', $new['select'] );
		} elseif ( '' === $new && $old ) {
			delete_post_meta( $post_id, 'event_info', $old );
		}
	}
}

add_action( 'save_post', 'save_event_info_meta' );

include plugin_dir_path(__FILE__) . '/includes/widgets/custom-events-widget.php';

include plugin_dir_path( __FILE__) . '/includes/shortcodes/custom-events-shortcode.php';
